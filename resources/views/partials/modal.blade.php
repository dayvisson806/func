<!-- Modal -->
<div class="modal fade" id="{{$id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="btn-dismiss" data-dismiss="modal">{{$btn-dismiss-label }}</button>
                <button type="button" class="btn btn-primary" id="btn-ok">{{$btn-ok-label }}</button>
            </div>
        </div>
    </div>
</div>