<div class="form-group">

    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null,array( 'class' =>'form-control','placeholder'=>'Digite seu nome')) !!}

</div>
<div class="form-group">
    {!! Form::label('email', 'Email') !!}
    {!! Form::email('email', null,array( 'class' =>'form-control','placeholder'=>'Digite seu email')) !!}
</div>
<div class="form-group">
    {!! Form::label('setor', 'Setor') !!}
    {!! Form::text('setor', null,array( 'class' =>'form-control','placeholder'=>'Digite seu setor')) !!}
</div>

<div class="form-group">
    {!! Form::label('cargo', 'Cargo') !!}
    {!! Form::text('cargo', null,array( 'class' =>'form-control','placeholder'=>'Digite seu cargo')) !!}
</div>

<div class="form-group">
    {!! Form::label('foto', 'Foto') !!}
    {!! Form::file('foto') !!}
    <p class="help-block">Imagem do tipo: jpeg, bmp, png. </p>
</div>

