@extends('layout')


@section('title')
    - Lista
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Lista de Funcionários</div>

                    <div class="panel-body">

                        {!! Form::model($busca,array('route' => 'funcionarios.store','method'=>'GET','class'=>'navbar-form navbar-left pull-right')) !!}

                        <div class="form-group">
                            {!! Form::text('nome', null,array( 'class' =>'form-control','placeholder'=>'Nome')) !!}
                            {!! Form::select('setor', $setores ,null,array( 'class' =>'form-control')) !!}

                        </div>
                        <button type="submit" class="btn btn-default">Buscar</button>
                        {!! Form::close() !!}

                        <p>
                            <a class="btn btn-primary" href="{{ route('funcionarios.create') }}">Novo</a>
                        </p>
                        @if(sizeof($funcionarios)>0)
                            <table class="table table-striped">
                                <tr>
                                    <th>#</th>
                                    <th>Foto</th>
                                    <th>Nome</th>
                                    <th>Email</th>
                                    <th>Setor</th>
                                    <th>Cargo</th>
                                    <th>Ações</th>
                                </tr>


                                @foreach($funcionarios as $funcionario)
                                    <tr>

                                        <td>{{$funcionario->id}}</td>
                                        <td><img class="img-responsive img-thumbnail" width="100px"  src="{{url('getfoto',$funcionario->id)}}"></td>
                                        <td>{{$funcionario->nome}}</td>
                                        <td>{{$funcionario->email}}</td>
                                        <td>{{$funcionario->setor}}</td>
                                        <td>{{$funcionario->cargo}}</td>
                                        <td>
                                            <a href="{!! route('funcionarios.edit',$funcionario->id) !!}" class="btn btn-default">Editar</a>
                                            <button class="btn btn-default" data-toggle="modal" data-target="#confirmationModal" data-func-id="{{$funcionario->id}}" data-func-nome="{{$funcionario->nome}}">Excluir</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        @else
                            <p class="text-center">Nenhum funcionário para exibir!</p>
                            <div ><span class="center-block" ></span></div>


                        @endif



                        <div class="pull-right">{!! $funcionarios->render()  !!}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('partials.modal-small',['id'=>'confirmationModal','btnDismissLabel'=>'Não','btnModalOklabel'=>'Sim'])

    {!! Form::open(['route'=> ['funcionarios.destroy',':FUNCIONARIO_ID'],'method'=>'DELETE','id'=>'form-delete']) !!}
    {!! Form::close() !!}

@endsection




@section('scripts')

    <script>

        $( document ).ready(function() {

            var rowDelete;
            var funcIdDelete;
            var funcNomeDelete;
            $('#confirmationModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal

                rowDelete = button.parents('tr');
                funcIdDelete = button.data('func-id')
                funcNomeDelete= button.data('func-nome')


                var modal = $(this)
                modal.find('.modal-title').text('Excluir ' + funcNomeDelete+'?')
                modal.find('.modal-body').text('Deseja realmente exluir ' + funcNomeDelete+'?');
            });

            $('#confirmationModal #btn-ok').click(function () {

                var form = $('#form-delete');
                var url = form.attr('action').replace(':FUNCIONARIO_ID',funcIdDelete);

                var data = form.serialize();

                $.post(url,data,function(message){

                    rowDelete.fadeOut();

                    $('#flash-message').load('{!! url('flash') !!}');

                    console.log(message);
                });

                console.log(data);

            });

        });
    </script>

@endsection


