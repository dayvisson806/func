@extends('layout')


@section('title')
    - Criar
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Novo Funcionário</div>

                    <div class="panel-body">
                        @include('funcionarios.partials.form-errors')


                        {!! Form::open(array('route' => 'funcionarios.store','files' => true)) !!}

                        @include('funcionarios.partials.form')

                        <button type="submit" class="btn btn-primary">Salvar</button>
                        <a class="btn btn-default" href="{{ route('funcionarios.index') }}" role="button">Cancelar</a>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



