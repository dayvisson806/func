@extends('layout')


@section('title')
    - Editar
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Editar Funcionário: {{$funcionario->nome}} </div>

                    <div class="panel-body">
                        @include('funcionarios.partials.form-errors')

                        {!! Form::model($funcionario,['route' => ['funcionarios.update' ,$funcionario], 'method'=>'PUT','files' => true]) !!}

                        @include('funcionarios.partials.form')
                        <button type="submit" class="btn btn-primary">Salvar</button>
                        <a class="btn btn-default" href="{{ route('funcionarios.index') }}" role="button">Cancelar</a>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



