<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'success' => array(
            'update' =>   '<strong>:item</strong> atualizado com sucesso!',
            'store'=> '<strong>:item</strong> criado com sucesso!',
            'destroy'=> '<strong>:item</strong> excluído com sucesso!',
    ),

);