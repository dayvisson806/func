<?php
use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: Dayvisson
 * Date: 09/04/2015
 * Time: 19:41
 */

class UserTableSeeder extends  Seeder {

    public function run(){

        \DB::table('users')->insert(array(
            'name'=>'Dayvisson Valadão',
            'email' => 'dayvisson806@gmail.com',
            'password' => \Hash::make('1234')
        ));
    }

}