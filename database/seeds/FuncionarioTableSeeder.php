<?php
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
/**
 * Created by PhpStorm.
 * User: Dayvisson
 * Date: 09/04/2015
 * Time: 19:41
 */

class FuncionarioTableSeeder extends  Seeder {

    public function run(){

        $faker = Faker::create();


        \DB::table('funcionarios')->insert(array(
            'nome'=>'Pedro',
            'email'=> 'pedro@dj.emp.br',
            'setor'=> 'TI',
            'cargo' =>'Gerente',
            'foto' => ''
        ));
        \DB::table('funcionarios')->insert(array(
            'nome'=>'João',
            'email'=> 'joao@dj.emp.br',
            'setor'=> 'TI',
            'cargo'=> 'Programador',
            'foto' => ''
        ));
        \DB::table('funcionarios')->insert(array(
            'nome'=>'Flavio',
            'email'=> 'flavio@dj.emp.br',
            'setor'=> 'TI',
            'cargo'=> 'Estagiário',
            'foto' => ''
        ));
        \DB::table('funcionarios')->insert(array(
            'nome'=>'Maria',
            'email'=> 'maria@dj.emp.br',
            'setor'=> 'Administrativo',
            'cargo'=> 'Secretária',
            'foto' => ''
        ));
        \DB::table('funcionarios')->insert(array(
            'nome'=>'Amanda',
            'email'=> 'amanda@dj.emp.br',
            'setor'=> 'Administrativo',
            'cargo' =>'Estagiária',
            'foto' => ''
        ));
        \DB::table('funcionarios')->insert(array(
            'nome'=>'Manoel',
            'email'=> 'manoel@dj.emp.br',
            'setor'=> 'Financeiro',
            'cargo'=> 'Contador',
            'foto' => ''
        ));
        \DB::table('funcionarios')->insert(array(
            'nome'=>'Laura',
            'email'=> 'laura@dj.emp.br',
            'setor'=> 'RH',
            'cargo'=> 'Psicóloga',
            'foto' => ''
        ));
        \DB::table('funcionarios')->insert(array(
            'nome'=>'Debora',
            'email'=> 'debora@dj.emp.br',
            'setor'=> 'RH',
            'cargo'=> 'Gerente',
            'foto' => ''
        ));
        \DB::table('funcionarios')->insert(array(
            'nome'=>'Bruno',
            'email'=> 'bruno@dj.emp.br',
            'setor'=> 'Diretoria',
            'cargo'=> 'CEO',
            'foto' => ''
        ));
        \DB::table('funcionarios')->insert(array(
            'nome'=>'Rodrigo',
            'email'=> 'rodrigo@dj.emp.br',
            'setor'=> 'Diretoria',
            'cargo'=> 'CFO',
            'foto' => ''
        ));



    }

}