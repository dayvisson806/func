<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Funcionario extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'funcionarios';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nome', 'email', 'setor','cargo','foto','foto_mime'];


    public function  scopeNome($query,$nome){

        if($nome != ""){

            $query->where('nome','LIKE','%'.$nome.'%');
        }
    }

    public function scopeSetor($query,$setor){



        $setores = Funcionario::setores();

        if($setor!=''&&isset($setores[$setor])){

            return $query->where('setor',$setor);
        }
    }

    public static  function setores(){
        $result = \DB::table('funcionarios')->select('setor')->distinct()->get();

        $setores = array();
        foreach($result as $s) {
            $setores[$s->setor] = $s->setor;
        }
        return $setores;
    }

}
