<?php
use Illuminate\Http\Response;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('flash', function()
{
   return view('partials.flash-message');
});

Route::get('getfoto/{id}',function($id) {


    $funcionario = \App\Funcionario::findOrFail($id);

    $mime = 'image/png';
    $foto  ='default_profile_picture.png';

    if ($funcionario->foto != ''){
        $foto =     $funcionario->foto;
        $mime =  $funcionario->foto_mime;
    }

    $file = Storage::disk('local')->get($foto);

    return (new Response($file, 200))
        ->header('Content-Type',$mime);

});

Route::resource('funcionarios','FuncionarioController');