<?php namespace App\Http\Controllers;

use App\Funcionario;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Storage;

class FuncionarioController extends Controller {


    /**
     * @var Request
     */
    private $funcionario;

    public function __construct(){

        $this->middleware('auth');
        $this->beforeFilter('@findFuncionario',['only'=>['show','edit','update','destroy']]);
    }


    public function findFuncionario(Route $route){
        $this->funcionario = Funcionario::findOrFail($route->getParameter('funcionarios'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Route $route,Request $request)
    {

        $busca= array(
            'nome' => $request->get('nome'),
            'setor' => $request->get('setor')
        );


        $funcionarios = Funcionario::nome($busca['nome'])->setor($busca['setor'])->paginate(5);
        $funcionarios->setPath($route->getPath());


        $setores =array('' => 'Todos Setores');
        $setores =  array_merge($setores,Funcionario::setores());

        return   view('funcionarios.index',compact('funcionarios','setores','busca'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return   view('funcionarios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $rules = array(
            'nome' =>'required',
            'email' =>'required|email|unique:funcionarios,email',
            'setor' =>'required',
            'cargo' =>'required',
            'foto' =>'mimes:jpeg,bmp,png'
        );




        $this->validate($request,$rules);

        $data = $request->all();

        if($request->hasFile('foto')){
            $file = $request->file('foto');
            $extension = $file->getClientOriginalExtension();
            Storage::disk('local')->put($file->getFilename().'.'.$extension,  \File::get($file));

            $data['foto'] =  $file->getFilename().'.'.$extension;
            $data['foto_mime'] = $file->getClientMimeType();
        }


        $funcionario = Funcionario::create($data);



        Session::flash('message', trans('messages.success.store',['item'=>$funcionario->nome]));



        return redirect()->route('funcionarios.index');




    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

        return   view('funcionarios.edit')->with('funcionario',$this->funcionario);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {


        $rules = array(
            'nome' =>'required',
            'email' =>'required|email|unique:funcionarios,email,'.$this->funcionario->id,
            'setor' =>'required',
            'cargo' =>'required',
            'foto' =>'mimes:jpeg,bmp,png'
        );




        $this->validate($request,$rules);

        $data = $request->all();

        if($request->hasFile('foto')){

            if ($this->funcionario->foto!=''&&Storage::exists($this->funcionario->foto))
            {
                Storage::delete($this->funcionario->foto);
            }


            $file = $request->file('foto');
            $extension = $file->getClientOriginalExtension();
            Storage::disk('local')->put($file->getFilename().'.'.$extension,  \File::get($file));

            $data['foto'] =  $file->getFilename().'.'.$extension;
            $data['foto_mime'] = $file->getClientMimeType();
        }





        $this->funcionario->fill($data);
        $this->funcionario->save();



        Session::flash('message', trans('messages.success.update',['item'=>$this->funcionario->nome]));



        return redirect()->route('funcionarios.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->funcionario->delete();

        Session::flash('message',  trans('messages.success.destroy',['item'=>$this->funcionario->nome]));


    }

}
